# stack_calculator.py

class StackCalculator:

    def __init__(self):
        self.stack = []

    def push(self, value):
        self.stack.append(value)

    def _operator(self, func):
        if len(self.stack) < 2:
            print("No enough arguments on stack")
            exit(1)
        second_arg = self.stack.pop()
        first_arg = self.stack.pop()
        self.push(func(first_arg, second_arg))

    def add(self):
        self._operator(lambda a, b: a+b)

    def sub(self):
        self._operator(lambda a, b: a-b)

    def mul(self):
        self._operator(lambda a, b: a*b)

    def div(self):
        if self.top() == 0:
            print("Division by zero")
            exit(1)
        self._operator(lambda a, b: a/b)

    def top(self):
        return self.stack[-1]
