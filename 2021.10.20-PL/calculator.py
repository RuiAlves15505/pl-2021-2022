# calculator.py

import ply.lex as lex
from stack_calculator import StackCalculator

# tokens
tokens = ("NUM", "ADD", "MUL", "DIV", "SUB")


# regex que definem os tokens
def t_NUM(t):
    r"""[0-9]+(\.[0-9]+)?"""
    t.value = float(t.value)
    return t

def t_ADD(t):
    "ADD"
    t.value = StackCalculator.add
    return t

def t_SUB(t):
    "SUB"
    t.value = StackCalculator.sub
    return t

def t_MUL(t):
    "MUL"
    t.value = StackCalculator.mul
    return t

def t_DIV(t):
    "DIV"
    t.value = StackCalculator.div
    return t

# carateres a serem ignorados
t_ignore = " "


def t_error(t):
    print(f"Unexpected token {t.value[:10]}...")
    exit(1)


lexer = lex.lex()
for line in iter(lambda: input(">> "), ""):
    lexer.input(line)
    stack = StackCalculator()
    for token in iter(lexer.token, None):
        if token.type == "NUM":
            stack.push(token.value)
        else:
            token.value(stack)
    print(stack.top())


