#!/usr/bin/env python3
import ply.lex as plex
from my_lib import slurp
import sys

if len(sys.argv) != 2:
    print("Expected HTML filename")
    exit(1)

filename = sys.argv[1]

tokens = ("TAGSLINE", "TAG", "TEXT", "NLNL")

def t_NLNL(t):
    r"""\n([ ]*\n)+"""
    print()
    pass

# texto     <tag>    <tag>   \n
def t_TAGSLINE(t):
    r"""([ ]*<[^>]+>)+[ ]*\n"""
    pass

def t_TAG(t):
    r"""<[^>]+>"""
    pass


def t_TEXT(t):
    r"""[^<]"""
    print(t.value, end="")
    pass


def t_error(t):
    print(f"Unexpected token!? '{t.value[:10]}'")
    exit(1)


lexer = plex.lex()
lexer.input(slurp(filename))
lexer.token()
