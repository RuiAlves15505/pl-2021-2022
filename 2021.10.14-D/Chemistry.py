# Chemistry.py

import ply.lex as lex

tokens = ("SQ", "NR")

t_SQ = r"Cl|C|H|O"
t_ignore = " "    # string de carateres a ignorar
# def t_SPC(t):
#     r"[ ]"
#     pass

def t_NR(t):
    r"[0-9]+"
    t.value = int(t.value)
    return t

def t_error(t):
    print(f"Token not recognized: {t}")
    exit(1)


lexer = lex.lex()
lexer.input("H2O HCl")
for t in iter(lexer.token, None):
    print(t)

