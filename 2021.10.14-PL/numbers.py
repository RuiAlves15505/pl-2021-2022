# numbers.py
Nrs = {str(x) for x in range(0, 10)}
V = {"+", "-", "."}.union(Nrs)
Q = ["A", "B", "C", "D", "E"]
q0 = "A"
F = {"C", "E"}      # set(["C","E"])
# A => { "+" => "B", "-" => "B", ... }, B => { "+" => ...

TT = {x: {v: "ERROR" for v in V} for x in Q}
# for x in Q:
#     TT[x] = {}
#     for v in V:
#         TT[x][v] = "ERROR"

for n in Nrs:
    for q in ["A", "B", "C"]:
        TT[q][n] = "C"
    for q in ["D", "E"]:
        TT[q][n] = "E"

TT["A"]["+"] = "B"
TT["A"]["-"] = "B"
TT["C"]["."] = "D"


def read_string(string):
    alpha = q0
    while len(string) > 0 and alpha != "ERROR":
        current_char = string[0]
        alpha = TT[alpha][current_char] if current_char in V else "ERROR"
        string = string[1:]
    return alpha in F and len(string) == 0


for teste in ["+3.14", "42", "-54", "-334.41", "5batatas", "cebolas", "1.2.3"]:
    print(f"{teste} => {read_string(teste)}")
