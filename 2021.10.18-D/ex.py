# ex.py
from my_utils import slurp
import ply.lex as lex

tokens = ("NEWLINE", "OTHER", "UPPERCASE", "LOWERCASE", "DIGIT", "SPACES")
counts = {}


def t_SPACES(t):
    r"[ ]"
    global counts
    counts["spaces"] = counts.get("spaces", 0) + 1
    pass


def t_DIGITS(t):
    r"[0-9]"
    global counts
    counts["digits"] = counts.get("digits", 0) + 1
    pass


def t_LOWERCASE(t):
    r"[a-z]"
    global counts
    counts["lowercases"] = counts.get("lowercases", 0) + 1
    pass


def t_UPPERCASE(t):
    r"[A-Z]"
    global counts
    counts["uppercases"] = counts.get("uppercases", 0) + 1
    pass


def t_OTHER(t):
    r"."
    global counts
    counts["others"] = counts.get("others", 0) + 1
    pass


def t_NEWLINE(t):
    r"\n"
    global counts
    counts["newlines"] = counts.get("newlines", 0) + 1
    pass


def t_error(t):
    print(f"Token not recognized: {t.value[:10]}")
    exit(1)


lexer = lex.lex()
lexer.input(slurp("app-abde.txt"))
lexer.token()

for token_type in counts.keys():
    print(f"Number of {token_type}: {counts[token_type]}")


